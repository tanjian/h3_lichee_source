
#include "de_lcd_type.h"
#include "de_lcd.h"

#include "disp_al.h"
#include "de_hal.h"

static volatile __de_lcd_dev_t *lcd_dev[2];

void simple_tcon_set_reg_base(void)
{
	lcd_dev[0]=(__de_lcd_dev_t *)(LCD0_BASE + 0 * 0x1000);
	lcd_dev[1]=(__de_lcd_dev_t *)(LCD0_BASE + 1 * 0x1000);
}

s32 simple_tcon_init(u32 sel)
{
	//tcon0_close(sel);
	lcd_dev[sel]->tcon0_ctl.bits.tcon0_en = 0;
	lcd_dev[sel]->tcon1_ctl.bits.tcon1_en = 0;
	//lcd_dev[sel]->tcon0_dclk.bits.tcon0_dclk_en = 0xf;
	lcd_dev[sel]->tcon_gctl.bits.tcon_en = 0;
	lcd_dev[sel]->tcon_gint0.bits.tcon_irq_en = 0;
	lcd_dev[sel]->tcon_gint0.bits.tcon_irq_flag = 0;
	lcd_dev[sel]->tcon_gctl.bits.tcon_en = 1;
	return 0;
}

s32 simple_tcon1_cfg(u32 sel,disp_video_timings* timing)
{
	u32 start_delay;

	lcd_dev[sel]->tcon1_basic0.bits.x = timing->x_res - 1;
	lcd_dev[sel]->tcon1_basic0.bits.y = timing->y_res / (timing->b_interlace+1) - 1;
	lcd_dev[sel]->tcon1_basic1.bits.ls_xo = timing->x_res - 1;
	lcd_dev[sel]->tcon1_basic1.bits.ls_yo = timing->y_res / (timing->b_interlace+1) + timing->vactive_space - 1;
	lcd_dev[sel]->tcon1_basic2.bits.xo = timing->x_res - 1;
	lcd_dev[sel]->tcon1_basic2.bits.yo = timing->y_res / (timing->b_interlace+1) + timing->vactive_space - 1;
	lcd_dev[sel]->tcon1_basic3.bits.ht = timing->hor_total_time-1;
	lcd_dev[sel]->tcon1_basic3.bits.hbp = timing->hor_sync_time+timing->hor_back_porch-1;
	lcd_dev[sel]->tcon1_basic4.bits.vt = timing->ver_total_time*(2 - timing->b_interlace)*((timing->vactive_space!=0)? 2:1);
	lcd_dev[sel]->tcon1_basic4.bits.vbp = timing->ver_sync_time+timing->ver_back_porch-1;
	lcd_dev[sel]->tcon1_basic5.bits.hspw = timing->hor_sync_time-1;
	lcd_dev[sel]->tcon1_basic5.bits.vspw = timing->ver_sync_time-1;
	lcd_dev[sel]->tcon1_io_pol.bits.io0_inv = timing->ver_sync_polarity;
	lcd_dev[sel]->tcon1_io_pol.bits.io1_inv = timing->hor_sync_polarity;
	lcd_dev[sel]->tcon1_ctl.bits.interlace_en = timing->b_interlace;
	lcd_dev[sel]->tcon_fill_start0.bits.fill_begin = (timing->ver_total_time + 1) << 12;
	lcd_dev[sel]->tcon_fill_end0.bits.fill_end = (timing->ver_total_time + timing->vactive_space) << 12;
	lcd_dev[sel]->tcon_fill_data0.bits.fill_value = 0;
	lcd_dev[sel]->tcon_fill_ctl.bits.tcon1_fill_en = (timing->vactive_space!=0)? 1:0;
	start_delay = (timing->ver_total_time - timing->y_res) / (timing->b_interlace+1) - 5;
	start_delay = (start_delay > 31)? 31:start_delay;
	lcd_dev[sel]->tcon1_ctl.bits.start_delay = start_delay;
	return 0;
}



s32 simple_tcon1_set_timming(u32 sel, disp_video_timings *timming)
{
	simple_tcon1_cfg(sel, timming);
	//tcon1_hdmi_color_remap(sel);
	lcd_dev[sel]->tcon1_io_pol.bits.io2_inv = 1;
	lcd_dev[sel]->tcon1_io_tri.bits.io0_output_tri_en = 1;
	lcd_dev[sel]->tcon1_io_tri.bits.io1_output_tri_en = 1;
	lcd_dev[sel]->tcon1_io_tri.bits.io2_output_tri_en = 1;
	lcd_dev[sel]->tcon1_io_tri.bits.io3_output_tri_en = 1;
	lcd_dev[sel]->tcon1_io_tri.bits.data_output_tri_en = 0xffffff;

	//hmdi_src_sel(sel);
	lcd_dev[0]->tcon_mul_ctl.bits.hdmi_src = sel;
	return 0;
}

s32 simple_tcon1_hdmi_color_remap(u32 sel,u32 onoff)
{
	lcd_dev[sel]->tcon_ceu_coef_rr.bits.value = 0;
	lcd_dev[sel]->tcon_ceu_coef_rg.bits.value = 0;
	lcd_dev[sel]->tcon_ceu_coef_rb.bits.value = 0x100;
	lcd_dev[sel]->tcon_ceu_coef_rc.bits.value = 0;

	lcd_dev[sel]->tcon_ceu_coef_gr.bits.value = 0x100;
	lcd_dev[sel]->tcon_ceu_coef_gg.bits.value = 0;
	lcd_dev[sel]->tcon_ceu_coef_gb.bits.value = 0;
	lcd_dev[sel]->tcon_ceu_coef_gc.bits.value = 0;

	lcd_dev[sel]->tcon_ceu_coef_br.bits.value = 0;
	lcd_dev[sel]->tcon_ceu_coef_bg.bits.value = 0x100;
	lcd_dev[sel]->tcon_ceu_coef_bb.bits.value = 0;
	lcd_dev[sel]->tcon_ceu_coef_bc.bits.value = 0;

	lcd_dev[sel]->tcon_ceu_coef_rv.bits.max = 235;
	lcd_dev[sel]->tcon_ceu_coef_rv.bits.min = 16;
	lcd_dev[sel]->tcon_ceu_coef_gv.bits.max = 235;
	lcd_dev[sel]->tcon_ceu_coef_gv.bits.min = 16;
	lcd_dev[sel]->tcon_ceu_coef_bv.bits.max = 235;
	lcd_dev[sel]->tcon_ceu_coef_bv.bits.min = 16;

	if(onoff)
		lcd_dev[sel]->tcon_ceu_ctl.bits.ceu_en = 1;
	else
		lcd_dev[sel]->tcon_ceu_ctl.bits.ceu_en = 0;
	return 0;
}

s32 simple_tcon1_open(u32 sel)
{
	lcd_dev[sel]->tcon1_ctl.bits.tcon1_en = 1;
	return 0;
}

int simple_hdmi_cfg(u32 screen_id, disp_video_timings *video_info)
{
	simple_tcon_init(screen_id);
	simple_tcon1_set_timming(screen_id, video_info);
	simple_tcon1_hdmi_color_remap(screen_id,1);
	return 0;
}

int simple_hdmi_enable(u32 screen_id)
{
	simple_tcon1_open(screen_id);
	return 0;
}

