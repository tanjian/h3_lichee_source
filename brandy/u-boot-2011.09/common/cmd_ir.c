#include <common.h>
#include <config.h>
#include <command.h>
#include <sunxi_mbr.h>
#include <boot_type.h>


extern int check_ir_boot_recovery(void);

int do_ir_boot_recovery(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    check_ir_boot_recovery();
    return 0;
}

U_BOOT_CMD(
	ir_recovery, CONFIG_SYS_MAXARGS, 1, do_ir_boot_recovery,
	"do a burn from boot",
	"pburn [mode]"
	"NULL"
);
