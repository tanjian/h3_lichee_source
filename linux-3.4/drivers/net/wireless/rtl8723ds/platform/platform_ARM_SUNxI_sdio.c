/******************************************************************************
 *
 * Copyright(c) 2013 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/
/*
 * Description:
 *	This file can be applied to following platforms:
 *	CONFIG_PLATFORM_ARM_SUNxI
 */
#include <drv_types.h>
#ifdef CONFIG_GPIO_WAKEUP
#include <linux/gpio.h>
#endif
#include <linux/platform_device.h>
#include <mach/sys_config.h>
#include <linux/gpio.h>

#ifdef CONFIG_MMC
#if defined(CONFIG_PLATFORM_ARM_SUNxI)
extern void sunxi_mci_rescan_card(unsigned id, unsigned insert);
extern int  sunxi_usb_disable_hcd(__u32 usbc_no);
extern int  sunxi_usb_enable_hcd(__u32 usbc_no);
extern int  sunxi_gpio_req(struct gpio_config *gpio);
//extern int  rf_module_power(int onoff);
extern void wifi_pm_power(int on);
extern int  rf_pm_gpio_ctrl(char *name, int level);

#endif
#ifdef CONFIG_GPIO_WAKEUP
extern unsigned int oob_irq;
#endif
#endif // CONFIG_MMC

/*
 * Return:
 *	0:	power on successfully
 *	others: power on failed
 */
int platform_wifi_power_on(void)
{
	int ret = 0;
        script_item_u val;
        script_item_value_type_e type;
#ifdef CONFIG_MMC
{

#if defined(CONFIG_PLATFORM_ARM_SUNxI)
        type = script_get_item("wifi_para", "wifi_sdc_id", &val);
	if (SCIRPT_ITEM_VALUE_TYPE_INT != type) {
                printk(KERN_ERR "failed to fetch sdio card's sdcid\n");
                return -1;
        }
        int wlan_bus_index = val.val;
        printk(KERN_ERR "wlan bus index = %d\n",wlan_bus_index);
        wifi_pm_power(1);
	mdelay(200);
        sunxi_mci_rescan_card(wlan_bus_index,1);
#endif
//	DBG_871X("%s: power up, rescan card.\n", __FUNCTION__);

#ifdef CONFIG_GPIO_WAKEUP
#if defined(CONFIG_PLATFORM_ARM_SUNxI)
        type = script_get_item("wifi_para", "wl_host_wake", &val);
	if (SCIRPT_ITEM_VALUE_TYPE_PIO != type) {
                printk(KERN_ERR "failed to fetch wl_host_wake\n");
                return -1;
        }
        oob_irq = val.gpio.gpio;
        printk(KERN_ERR "oob_irq = %d\n",oob_irq);

#endif
#endif // CONFIG_GPIO_WAKEUP
}
#endif // CONFIG_MMC

	return ret;
}

void platform_wifi_power_off(void)
{
	script_item_u val;
        script_item_value_type_e type;

#ifdef CONFIG_MMC
#if defined(CONFIG_PLATFORM_ARM_SUNxI)
	type = script_get_item("wifi_para", "wifi_sdc_id", &val);
        if (SCIRPT_ITEM_VALUE_TYPE_INT != type) {
                printk(KERN_ERR "failed to fetch sdio card's sdcid\n");
                return -1;
        }
        int wlan_bus_index = val.val;

        sunxi_mci_rescan_card(wlan_bus_index,0);
	mdelay(200);
	wifi_pm_power(0);
#endif
//	DBG_871X("%s: remove card, power off.\n", __FUNCTION__);
#endif // CONFIG_MMC
}
